﻿using System.Drawing;

namespace L1
{
    public abstract class Shape: IDrawable
    {
        protected Point[] points;
        public double Area { get; protected set; }
        public string Name { get; protected set; }

        public abstract void Draw(Color color, Graphics graphics);

        public override string ToString()
        {
            return Name + ": " + Area.ToString("F2");
        }
    }
}
