﻿using System.Drawing;

namespace L1
{
    class Line : Shape
    {
        public Line(int x1, int y1, int x2, int y2)
        {
            points = new Point[2];
            points[0] = new Point(x1, y1);
            points[1] = new Point(x2, y2);
            Name = "Line";
            Area = 0;
        }

        public override void Draw(Color color, Graphics graphics)
        {
            using (var pen = new Pen(color))
            {
                graphics.DrawLine(pen, points[0].X, points[0].Y, points[1].X, points[1].Y);
            }
        }
    }
}
