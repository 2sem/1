﻿using System;
using System.Drawing;

namespace L1
{
    class Circle : Shape
    {
        private int radius;

        public Circle(int radius, Point center)
        {
            this.radius = radius;
            points = new Point[1];
            points[0] = center;
            Name = "Circle";
            Area = Math.PI * radius * radius;
        }

        public override void Draw(Color color, Graphics graphics)
        {
            using (var brush = new SolidBrush(color))
            {
                graphics.FillEllipse(brush, new RectangleF(points[0].X - radius, points[0].Y - radius, radius * 2, radius * 2));
            }
        }
    }
}
