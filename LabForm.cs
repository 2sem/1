﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace L1
{
    public partial class LabForm : Form
    {
        private readonly Collection<IDrawable> shapes;

        public LabForm()
        {
            InitializeComponent();

            shapes = new Collection<IDrawable>();
            shapes.Add(new Rectangle(10, 20, 500, 440));
            shapes.Add(new Circle(40, new Point(100, 200)));
            shapes.Add(new Line(10, 100, 200, 300));
            shapes.Add(new Triangle(300, 310, 400, 413, 357, 600));
            shapes.Add(new Ellipse(400, 400, 200, 100));
            shapes.Add(new Parallelogram(300, 20, 340, 420, 350, 130));

            string text = "";
            foreach (Shape shape in shapes)
            {
                text += shape + "\n";
            }
            MessageBox.Show(text, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        protected override void OnPaint(PaintEventArgs paintEventArgs)
        {
            base.OnPaint(paintEventArgs);
            var rand = new Random();
            const int Mask = unchecked((int)0xAA000000);
            using (var graphics = paintEventArgs.Graphics)
            {
                foreach (IDrawable shape in shapes)
                {
                    int color = Mask | rand.Next();
                    shape.Draw(Color.FromArgb(color), graphics);
                }
            }
        }
    }
}
