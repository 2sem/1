﻿using System.Drawing;

namespace L1
{
    class Rectangle : RectBoundedShape
    {
        public Rectangle(int x1, int y1, int x2, int y2) : base(x1, y1, x2, y2)
        {
            Area = width * height;
            Name = "Rectangle";
        }

        public override void Draw(Color color, Graphics graphics)
        {
            using (var brush = new SolidBrush(color))
            {
                graphics.FillRectangle(brush, new System.Drawing.Rectangle(points[0], new Size(width, height)));
            }
        }
    }
}
