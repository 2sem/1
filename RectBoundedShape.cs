﻿using System;
using System.Drawing;

namespace L1
{
    abstract class RectBoundedShape : Shape
    {
        protected int width;
        protected int height;

        protected RectBoundedShape(int x1, int y1, int x2, int y2)
        {
            points = new Point[1];
            points[0] = new Point((x2 > x1) ? x1 : x2, (y2 > y1) ? y1 : y2);
            width = Math.Abs(x2 - x1);
            height = Math.Abs(y2 - y1);
        }
    }
}
