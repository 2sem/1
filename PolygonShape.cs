﻿using System.Drawing;

namespace L1
{
    abstract class PolygonShape : Shape
    {
        public override void Draw(Color color, Graphics graphics)
        {
            using (var brush = new SolidBrush(color))
            {
                graphics.FillPolygon(brush, points);
            }
        }
    }
}
