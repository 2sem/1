﻿using System;
using System.Drawing;

namespace L1
{
    class Triangle : PolygonShape
    {
        public Triangle(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            points = new Point[] { new Point(x1, y1), new Point(x2, y2), new Point(x3, y3) };
            Name = "Triangle";
            double a = Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            double b = Math.Sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
            double c = Math.Sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2));
            double p = (a + b + c) / 2;
            Area = Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }
    }
}
