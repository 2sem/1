﻿using System.Drawing;

namespace L1
{
    interface IDrawable
    {
        public void Draw(Color color, Graphics graphics);
    }
}
