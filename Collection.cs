﻿using System;
using System.Collections.Generic;

namespace L1
{
    class Collection<T>
    {
        private const int DefaultSize = 4;
        private T[] data;
        public int Count { get; private set; }

        public Collection()
        {
            data = new T[DefaultSize];
            Count = 0;
        }

        public Collection(int size)
        {
            data = new T[size];
            Count = 0;
        }

        public void Add(T value)
        {
            if (data.Length == Count)
            {
                Array.Resize<T>(ref data, data.Length * 2);
            }
            data[Count++] = value;
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (var i = 0; i < Count; i++)
            {
                yield return data[i];
            }
        }

        public void Delete(int index)
        {
            if ((index < 0) || (index >= Count))
            {
                throw new IndexOutOfRangeException();
            }
            for (var i = index; i < Count; i++)
            {
                data[i] = data[i + 1];
            }
            Count--;
        }

        public T this[int index]
        {
            get
            {
                return data[index];
            }
            set
            {
                data[index] = value;
            }
        }
    }
}
