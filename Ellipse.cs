﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace L1
{
    class Ellipse : RectBoundedShape
    {
        public Ellipse(int x1, int y1, int x2, int y2) : base(x1, y1, x2, y2)
        {
            Area = width * height * Math.PI;
            Name = "Ellipse";
        }
        public override void Draw(Color color, Graphics graphics)
        {
            using (var brush = new SolidBrush(color))
            {
                graphics.FillEllipse(brush, new System.Drawing.Rectangle(points[0], new Size(width, height)));
            }
        }
    }
}
