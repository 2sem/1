﻿using System;
using System.Drawing;

namespace L1
{
    class Parallelogram : PolygonShape
    {
        public Parallelogram(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            points = new Point[] {
                new Point(x1, y1), new Point(x2, y2), new Point(x3, y3), new Point(x3 - (x2 - x1), y3 - (y2 - y1))
            };
            Name = "Parallelogram";
            double a = Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            double b = Math.Sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
            Area = a * b;
        }
    }
}
